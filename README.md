# [alexandergunnarson/slingshot](https://clojars.org/alexandergunnarson/slingshot)

Enhanced throw, try, leveraging Clojure's capabilities

![](https://qa.debian.org/cgi-bin/popcon-png?packages=libslingshot-clojure&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)